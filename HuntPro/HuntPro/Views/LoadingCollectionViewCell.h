//
//  LoadingCollectionViewCell.h
//  HuntPro
//
//  Created by Haoxin Li on 11/1/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *LoadingCollectionViewCellIdentifier = @"LoadingCollectionViewCell";

@interface LoadingCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *indicatorLoadig;
@property (nonatomic, weak) IBOutlet UILabel *lblEmptyMessage;

- (void) showEmptyMessage;
- (void) showEmptyMessage:(NSString *)message;
- (void) showLoadingIndicator;

@end
