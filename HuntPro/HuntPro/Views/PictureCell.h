//
//  PictureCell.h
//  HuntPro
//
//  Created by AOC on 29/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PictureCell : UICollectionViewCell

- (void) setImage : (NSString *) imgUrl;

@end
