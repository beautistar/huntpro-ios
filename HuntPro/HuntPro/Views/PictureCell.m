//
//  PictureCell.m
//  HuntPro
//
//  Created by AOC on 29/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PictureCell.h"
@import AFNetworking;

@interface PictureCell() {
    
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imvPic;

@end

@implementation PictureCell

- (void) setImage : (NSString *) imgUrl {    
   
    [_imvPic setImageWithURL:[NSURL URLWithString:imgUrl]];
    
}

@end
