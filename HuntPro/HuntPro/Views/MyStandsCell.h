//
//  MyStandsCell.h
//  HuntPro
//
//  Created by AOC on 28/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StandEntity.h"

@interface MyStandsCell : UITableViewCell

- (void) setStand : (StandEntity *) _stand ;
@end
