//
//  LoadingCollectionViewCell.m
//  HuntPro
//
//  Created by Haoxin Li on 11/1/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LoadingCollectionViewCell.h"

@implementation LoadingCollectionViewCell

- (void) showEmptyMessage {
    [self showEmptyMessage:@"Empty"];
}

- (void) showEmptyMessage:(NSString *)message {
    [self.indicatorLoadig stopAnimating];
    [self.lblEmptyMessage setText:message];
    [self.lblEmptyMessage setHidden:false];
}

- (void) showLoadingIndicator {
    [self.indicatorLoadig startAnimating];
    [self.lblEmptyMessage setText:nil];
    [self.lblEmptyMessage setHidden:true];
}

@end
