//
//  MyStandsCell.m
//  HuntPro
//
//  Created by AOC on 28/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "MyStandsCell.h"

@interface MyStandsCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imvState;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@end

@implementation MyStandsCell

- (void) setStand : (StandEntity *) _stand {
    
    _lblTitle.text = _stand._title;
    
    if (_stand._reservation._standUser._name.length != 0) {
        
        _lblUserName.text = _stand._reservation._standUser._name;
    
    } else {
        
        _lblUserName.text = @"Available";        
        
    }
}

@end
