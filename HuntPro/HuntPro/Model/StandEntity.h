//
//  StandEntity.h
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReservationEntity.h"
#import "LocationPointTypeEntity.h"

@interface StandEntity : NSObject

@property (nonatomic) int _id;

@property (nonatomic, strong) NSString *_title;

@property (nonatomic, strong) NSMutableArray *_locationpointTypes;

@property (nonatomic, strong) ReservationEntity *_reservation;

@end
