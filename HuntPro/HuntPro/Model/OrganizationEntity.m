//
//  OrganizationEntity.m
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "OrganizationEntity.h"

@implementation OrganizationEntity

@synthesize _organizationId, _organizationName, _organizationPictureUrl;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _organizationId = 0;
        _organizationName = @"";
        _organizationPictureUrl = @"";
    }
    
    return self;
}


@end
