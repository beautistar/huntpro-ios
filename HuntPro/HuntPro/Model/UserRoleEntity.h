//
//  UserRoleEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PermissionEntity.h"

@interface UserRoleEntity : NSObject

@property (nonatomic) int _id;

@property (nonatomic, strong) NSString *_name;

@property (nonatomic, strong) PermissionEntity *_picturePermissions;

@property (nonatomic, strong) PermissionEntity *_eventPermissions;

@property (nonatomic, strong) PermissionEntity *_accountPermissions;

@property (nonatomic, strong) PermissionEntity *_gamelogPermissions;

@property (nonatomic, strong) PermissionEntity *_mapPermissions;

@property (nonatomic, strong) PermissionEntity *_chatPermissions;



@end
