//
//  AccountEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserRoleEntity.h"
#import "ProfilePictureEntity.h"

@interface AccountEntity : NSObject

@property (nonatomic) int _accountId;

@property (nonatomic, strong) NSString * _accountName;

@property (nonatomic, strong) UserRoleEntity *_userRole;

@property (nonatomic, strong) ProfilePictureEntity *_profilePicture;

@property (nonatomic) int _memberStat;

@property (nonatomic) int _propertyStat;

@property (nonatomic) int _dataUsed;

// property entity obj array.
@property (nonatomic, strong) NSMutableArray *_properties;



@end
