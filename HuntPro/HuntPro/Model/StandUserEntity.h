//
//  StandUserEntity.h
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfilePictureEntity.h"

@interface StandUserEntity : NSObject

@property (nonatomic, strong) NSString * _id;

@property (nonatomic, strong) NSString *_name;

@property (nonatomic, strong) ProfilePictureEntity *_profilePicture;

@end
