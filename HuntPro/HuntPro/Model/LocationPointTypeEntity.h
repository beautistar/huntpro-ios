//
//  LocationPointTypeEntity.h
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationPointTypeEntity : NSObject

@property (nonatomic) int _id;

@property (nonatomic, strong) NSString *_name;

@end
