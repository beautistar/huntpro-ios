//
//  ReservationEntity.h
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StandUserEntity.h"

@interface ReservationEntity : NSObject

@property (nonatomic, strong) NSString *_id;

@property (nonatomic) int _locationId;

@property (nonatomic, strong) StandUserEntity *_standUser;

@property (nonatomic, strong) NSString *_startTime;

@property (nonatomic, strong) NSString *_endTime;

@end
