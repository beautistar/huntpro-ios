//
//  ProfilePictureEntity.m
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ProfilePictureEntity.h"

@implementation ProfilePictureEntity

@synthesize _pictureId, _size, _width, _height, _blobUrl, _thumbnails;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _pictureId = @"";
        _size = 0;
        _width = 0;
        _height = 0;
        _blobUrl = @"";
        _thumbnails = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end
