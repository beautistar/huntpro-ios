//
//  StandEntity.m
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "StandEntity.h"

@implementation StandEntity

@synthesize _id, _title, _reservation, _locationpointTypes;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _id = 0;
        _title = @"";
        _locationpointTypes = [[NSMutableArray alloc] init];
        _reservation = [[ReservationEntity alloc] init];
    }
    
    return self;
}

@end
