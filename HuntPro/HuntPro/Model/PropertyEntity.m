//
//  PropertyEntity.m
//  HuntPro
//
//  Created by AOC on 14/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PropertyEntity.h"

@implementation PropertyEntity

@synthesize _propertyId, _propertyName, _dataUsed, _zipCode;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _propertyId = 0;
        
        _propertyName = @"";
        
        _zipCode = @"";
        
        _dataUsed = 0;
        
    }
    
    return self;
}


@end
