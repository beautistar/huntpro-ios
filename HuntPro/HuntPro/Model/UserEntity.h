//
//  UserEntity.h
//  HuntPro
//
//  Created by AOC on 12/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfilePictureEntity.h"
#import "OrganizationEntity.h"
#import "WidgetEntity.h"
#import "AccountEntity.h"
#import "PropertyEntity.h"

@interface UserEntity : NSObject

@property (nonatomic, strong) NSString *_username;

@property (nonatomic, strong) NSString *_password;

@property (nonatomic, strong) NSString *_id;

@property (nonatomic, strong) NSString *_name;

@property (nonatomic, strong) ProfilePictureEntity *_profilePicture;

@property (nonatomic, strong) NSMutableArray *_claims;

@property (nonatomic, strong) NSMutableArray *_roles;

@property (nonatomic) BOOL _isSiteAdmin;

@property (nonatomic, strong) NSMutableArray *_accounts;

@property (nonatomic, strong) OrganizationEntity *_organization;

@property (nonatomic, strong) WidgetEntity *_webWidgets;

// for temp

@property (nonatomic) int _propertyId;



@end
