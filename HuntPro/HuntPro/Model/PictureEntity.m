//
//  PictureEntity.m
//  HuntPro
//
//  Created by AOC on 25/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PictureEntity.h"

@implementation PictureEntity

@synthesize _pictureId, _size, _taken, _title, _width, _height, _blogUrl, _created,_location, _tags;
@synthesize _modified, _property, _thumbnails, _description, _userPermission,_originalFileLocation;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _pictureId = @"";
        _title = @"";
        _description = @"";
        _property = [[PropertyEntity alloc] init];
        _created = @"";
        _modified = @"";
        _taken = @"";
        _size = 0;
        _height = 0;
        _width = 0;
        _blogUrl = @"";
        _thumbnails = [[NSMutableArray alloc] init];
        _tags = [[NSMutableArray alloc] init];
        _location = [[LocationEntity alloc] init];
        _userPermission = [[PermissionEntity alloc] init];
        _originalFileLocation = [[LocationEntity alloc] init];
        
    }
    
    return self;
}

@end
