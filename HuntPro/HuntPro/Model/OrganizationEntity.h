//
//  OrganizationEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrganizationEntity : NSObject

@property (nonatomic, strong) NSString *_organizationName;

@property (nonatomic) int _organizationId;

@property (nonatomic) NSString *_organizationPictureUrl;

@end
