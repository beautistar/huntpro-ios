//
//  LocationEntity.m
//  HuntPro
//
//  Created by AOC on 18/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LocationEntity.h"

@implementation LocationEntity

@synthesize _id, _title, _ownerId, _latitude, _longitude, _propertyId, _description;

- (instancetype) init {
    
    if (self == [super init] ) {
        
        _id = 0;
        _latitude = 0;
        _longitude = 0;
        _title = @"";
        _ownerId = @"";
        _propertyId = 0;
        _description = @"";
    }
    
    return self;
}

@end
