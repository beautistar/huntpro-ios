//
//  LocationPointTypeEntity.m
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "LocationPointTypeEntity.h"

@implementation LocationPointTypeEntity

@synthesize _id, _name;

- (instancetype) init {
    
    if(self == [super init]) {
        
        _id = 0;
        _name = @"";
        
    }
    
    return self;    
    
}

@end
