//
//  AccountEntity.m
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "AccountEntity.h"

@implementation AccountEntity

@synthesize _accountId, _accountName, _userRole, _profilePicture, _memberStat, _propertyStat, _dataUsed, _properties;


- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _accountId = 0;
        _accountName = @"";
        _userRole = [[UserRoleEntity alloc] init];
        _profilePicture = [[ProfilePictureEntity alloc] init];
        _memberStat = 0;
        _propertyStat = 0;
        _dataUsed = 0;
        _properties = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end
