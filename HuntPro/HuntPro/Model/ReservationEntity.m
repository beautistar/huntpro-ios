//
//  ReservationEntity.m
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ReservationEntity.h"

@implementation ReservationEntity

@synthesize _id, _endTime, _locationId, _standUser, _startTime;

- (instancetype) init {
    
    if (self == [super init]) {
        
        _id = @"";
        _locationId = 0;
        _standUser = [[StandUserEntity alloc] init];
        _startTime = @"";
        _endTime = @"";
    }
    
    return self;
}

@end
