//
//  ProfilePictureEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThumbnailEntity.h"

@interface ProfilePictureEntity : NSObject

@property (nonatomic, strong) NSString * _pictureId;

@property (nonatomic) int _size;

@property (nonatomic) int _width;

@property (nonatomic) int _height;

@property (nonatomic, strong) NSString *_blobUrl;

@property (nonatomic, strong) NSMutableArray *_thumbnails;

@end
