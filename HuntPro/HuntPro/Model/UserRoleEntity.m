//
//  UserRoleEntity.m
//  HuntPro
//


#import "UserRoleEntity.h"

@implementation UserRoleEntity

@synthesize _id, _name, _mapPermissions, _chatPermissions, _eventPermissions, _accountPermissions, _gamelogPermissions, _picturePermissions;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _id = 0;
        _name = @"";
        _picturePermissions = [[PermissionEntity alloc] init];
        _gamelogPermissions = [[PermissionEntity alloc] init];
        _accountPermissions = [[PermissionEntity alloc] init];
        _eventPermissions = [[PermissionEntity alloc] init];
        _chatPermissions = [[PermissionEntity alloc] init];
        
    }
    
    return self;
}

@end
