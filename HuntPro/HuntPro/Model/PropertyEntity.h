//
//  PropertyEntity.h
//  HuntPro
//
//  Created by AOC on 14/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyEntity : NSObject

@property (nonatomic) int _propertyId;

@property (nonatomic, strong) NSString * _propertyName;

@property (nonatomic, strong) NSString *_zipCode;

@property (nonatomic) int _dataUsed;

@end
