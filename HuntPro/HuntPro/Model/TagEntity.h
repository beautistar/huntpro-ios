//
//  TagEntity.h
//  HuntPro
//
//  Created by AOC on 21/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagEntity : NSObject

@property (nonatomic) int _accountId;

@property (nonatomic, strong) NSString *_title;

@property (nonatomic, strong) NSString *_description;

@property (nonatomic) int _userAccount;

@end
