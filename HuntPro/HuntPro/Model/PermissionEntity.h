//
//  PermissionEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PermissionEntity : NSObject

@property (nonatomic) BOOL _canView;

@property (nonatomic) BOOL _canEdit;

@property (nonatomic) BOOL _canShare;

@property (nonatomic) BOOL _canAdd;

@property (nonatomic) BOOL _canDelete;

@end
