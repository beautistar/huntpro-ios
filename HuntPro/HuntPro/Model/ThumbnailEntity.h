//
//  ThumbnailEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThumbnailEntity : NSObject

@property (nonatomic) int _thumbnailId;

@property (nonatomic, strong) NSString * _blobUrl;

@property (nonatomic) int _width;

@property (nonatomic) int _height;

@property (nonatomic) int _size;

@end
