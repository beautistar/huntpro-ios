//
//  LocationEntity.h
//  HuntPro
//
//  Created by AOC on 18/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationEntity : NSObject

@property (nonatomic) int _id;

@property (nonatomic) float _latitude;

@property (nonatomic) float _longitude;

@property (nonatomic, strong) NSString *_title;

@property (nonatomic, strong) NSString *_ownerId;

@property (nonatomic) int _propertyId;

@property (nonatomic, strong) NSString * _description;

@end
