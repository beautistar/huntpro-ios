//
//  StandUserEntity.m
//  HuntPro
//
//  Created by AOC on 16/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "StandUserEntity.h"

@implementation StandUserEntity

@synthesize _id, _name, _profilePicture;

- (instancetype) init {
    
    if (self == [super init]) {
        
        
        _id = @"";
        _name = @"";
        _profilePicture = [[ProfilePictureEntity alloc] init];
    }
    
    return self;
}

@end
