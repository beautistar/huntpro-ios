//
//  WidgetEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WidgetEntity : NSObject

@property (nonatomic, strong) NSString *_widget;

@end
