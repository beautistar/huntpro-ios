//
//  RoleEntity.h
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoleEntity : NSObject

@property (nonatomic, strong) NSString *_id;

@property (nonatomic, strong) NSString *_name;

@end
