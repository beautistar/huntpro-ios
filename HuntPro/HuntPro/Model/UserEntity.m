//
//  UserEntity.m
//  HuntPro
//
//  Created by AOC on 12/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _username, _password, _id, _name, _roles, _claims, _profilePicture, _accounts,  _webWidgets, _isSiteAdmin, _organization, _propertyId;


- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        

        _username = @"";
        _password = @"";

        
        _id = @"";
        _name = @"";
        _profilePicture = [[ProfilePictureEntity alloc] init];
        _claims = [[NSMutableArray alloc] init];
        _roles = [[NSMutableArray alloc] init];
        _isSiteAdmin = NO;
        _accounts = [[NSMutableArray alloc] init];
        _organization = [[OrganizationEntity alloc] init];
        _webWidgets = [[WidgetEntity alloc] init];
        
        //for temp
        _propertyId = 0;
    }
    
    return self;
}

@end
