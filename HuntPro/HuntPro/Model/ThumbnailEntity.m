//
//  ThumbnailEntity.m
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "ThumbnailEntity.h"

@implementation ThumbnailEntity

@synthesize _thumbnailId, _size, _width, _height, _blobUrl;


- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _thumbnailId = 0;
        _size = 0;
        _width = 0;
        _height = 0;
        _blobUrl = @"";
    }
    
    return self;
}



@end
