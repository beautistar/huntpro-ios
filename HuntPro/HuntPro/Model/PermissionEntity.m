//
//  PermissionEntity.m
//  HuntPro
//
//  Created by AOC on 13/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PermissionEntity.h"

@implementation PermissionEntity

@synthesize _canAdd, _canEdit, _canView, _canShare, _canDelete;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _canAdd = NO;
        _canEdit = NO;
        _canDelete = NO;
        _canShare = NO;
        _canView = NO;
    }
    
    return self;
}





@end
