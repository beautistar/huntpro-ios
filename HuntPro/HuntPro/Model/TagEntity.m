//
//  TagEntity.m
//  HuntPro
//
//  Created by AOC on 21/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "TagEntity.h"

@implementation TagEntity

@synthesize _accountId, _title, _description, _userAccount;

- (instancetype) init {
    
    if (self = [super init] ) {
        
        _accountId = 0;
        _title = @"";
        _description = @"";
        _userAccount = 0;
    }
    
    return self;
}

@end
