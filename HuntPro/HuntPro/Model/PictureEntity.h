//
//  PictureEntity.h
//  HuntPro
//
//  Created by AOC on 25/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PropertyEntity.h"
#import "ThumbnailEntity.h"
#import "TagEntity.h"
#import "LocationEntity.h"
#import "PermissionEntity.h"

@interface PictureEntity : NSObject

@property (nonatomic, strong) NSString * _pictureId;

@property (nonatomic, strong) NSString *_title;

@property (nonatomic, strong) NSString *_description;

@property (nonatomic, strong) PropertyEntity *_property;

@property (nonatomic, strong) NSString *_created;

@property (nonatomic, strong) NSString * _modified;

@property (nonatomic, strong) NSString * _taken;

@property (nonatomic) int _size;

@property (nonatomic) int _height;

@property (nonatomic) int _width;

@property (nonatomic, strong) NSString * _blogUrl;

@property (nonatomic, strong) NSMutableArray * _thumbnails;

@property (nonatomic, strong) NSMutableArray * _tags;

@property (nonatomic, strong) LocationEntity *_location;

@property (nonatomic, strong) PermissionEntity *_userPermission;

@property (nonatomic, strong) LocationEntity *_originalFileLocation;

@end
