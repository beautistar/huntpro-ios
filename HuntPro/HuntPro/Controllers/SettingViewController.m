//
//  SettingViewController.m
//  HuntPro
//
//  Created by AOC on 24/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "SettingViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>

@interface SettingViewController() {
    
    UserEntity *_user;
    
    AccountEntity *_selectedAccount;
    PropertyEntity *_selectedProperty;
    
    NSMutableArray * _accounts;
    NSMutableArray * _properties;
}

@property (weak, nonatomic) IBOutlet UITextField *tfUserAccount;
@property (weak, nonatomic) IBOutlet UITextField *tfPropertyId;
@property (weak, nonatomic) IBOutlet UIButton *btnAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnProperty;


@end

@implementation SettingViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    _accounts = [[NSMutableArray alloc] init];
    _properties = [[NSMutableArray alloc] init];    
    
    for (AccountEntity * _account in _user._accounts ) {
        
        [_accounts addObject:_account._accountName];
    }
}

- (IBAction)accountAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Account" rows:_accounts initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.tfUserAccount setText:selectedValue];
        _selectedAccount = _user._accounts[selectedIndex];
        
        for (PropertyEntity * property in _selectedAccount._properties) {
            
            [_properties addObject:property._propertyName];
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnAccount];
}

- (IBAction)propertyAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Property" rows:_properties initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.tfPropertyId setText:selectedValue];
        _selectedProperty = _selectedAccount._properties[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnProperty];
}

- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)okAction:(id)sender {
    
    ACCOUNTID = _selectedAccount._accountId;
    PROPERTYID = _selectedProperty._propertyId;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
