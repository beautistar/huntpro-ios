//
//  LoginViewController.m
//  HuntPro
//


#import "LoginViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"
#import "ProfilePictureEntity.h"
#import "RoleEntity.h"
#import "PermissionEntity.h"
#import "AccountEntity.h"
#import "UserRoleEntity.h"
#import "PropertyEntity.h"
#import "OrganizationEntity.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController () {
    
    UserEntity *_user;
    
    
}

@property (weak, nonatomic) IBOutlet UITextField *tfUserName;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end

@implementation LoginViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
//    BOOL isLoggedIn = [CommonUtils getUserLogin];
    
    // for test
//    if(isLoggedIn) {
//        
//        [self gotoMain];
//    }
    
    //for test
    NSString *name = @"jsamples@blueridgelabs.com";
    NSString *pwd = @"Testing1!";
    _tfUserName.text = name;
    _tfPassword.text = pwd;
       
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    return 220;
}

- (BOOL) isValid {
    
    if (_tfUserName.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERNAME positive:ALERT_OK negative:nil];
        return NO;
        
    } else if (_tfPassword.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:ALERT_OK negative:nil];
        return NO;
    }
    
    return YES;
    
}

- (IBAction)loginAction:(id)sender {
    
    if([self isValid]) {
        
        [self doLogin];
    }
}

- (void) doLogin {
    
    [self showLoadingViewWithTitle:nil];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_LOGIN];
    
    NSDictionary *params = @{
                             PARAM_USERNAME : _tfUserName.text,
                             PARAM_PASSWORD : _tfPassword.text,
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        [self hideLoadingView];
        
        NSLog(@"login respond : %@", responseObject);
        
        // success
        
        _user._id = [responseObject valueForKey:RES_ID];
        _user._name = [responseObject valueForKey:RES_NAME];
        _user._username = _tfUserName.text;
        _user._password = _tfPassword.text;
        
        [CommonUtils saveUserInfo];
        [CommonUtils setUserLogin:YES];
        
        id profileDic = [responseObject objectForKey:RES_PROFILE_PIC];
            
        ProfilePictureEntity *_profilePic = [[ProfilePictureEntity alloc] init];
        
        if(profileDic != [NSNull null]) {
            
            NSDictionary * _profileDic = (NSDictionary *) profileDic;
            
            _profilePic._pictureId = [_profileDic valueForKey:RES_PICTUREID];
            _profilePic._size = [[_profileDic valueForKey:RES_SIZE] intValue];
            _profilePic._width = [[_profileDic valueForKey:RES_WIDTH] intValue];
            _profilePic._height = [[_profileDic valueForKey:RES_HEIGHT] intValue];
            _profilePic._blobUrl = [_profileDic valueForKey:RES_BLOUURL];
            
            
            NSMutableArray *_thumbnails = [_profileDic valueForKey:RES_THUMNAILS];
            
            for (NSDictionary *_dict in _thumbnails) {
            
                ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
            
                _thumbnail._thumbnailId = [[_dict valueForKey:RES_THUMNAILID] intValue];
                _thumbnail._blobUrl = [_dict valueForKey:RES_BLOUURL];
                _thumbnail._width = [[_dict valueForKey:RES_WIDTH] intValue];
                _thumbnail._height = [[_dict valueForKey:RES_HEIGHT] intValue];
                _thumbnail._size = [[_dict valueForKey:RES_SIZE] intValue];
                
                [_profilePic._thumbnails addObject:_thumbnail];
            }
        }
        // user profile picture & claims
        _user._profilePicture = _profilePic;
        
        _user._claims = [responseObject valueForKey:RES_CLAIMS];
        
        NSMutableArray *_roles = [responseObject objectForKey:RES_ROLES];
        
        for (NSDictionary *_roleDic in _roles) {
        
            RoleEntity *_role = [[RoleEntity alloc] init];
            
            _role._id = [_roleDic valueForKey:RES_ID];
            _role._name = [_roleDic valueForKey:RES_NAME];
            
            [_user._roles addObject:_role];
        }
        
        //user isSiteAdmin
        _user._isSiteAdmin = [[responseObject valueForKey:RES_ISSITEADMIN] boolValue];
        
        //user accounts
        
        NSMutableArray *_accounts = [responseObject valueForKey:RES_ACCOUNTS];
        
        for (NSDictionary *_accountDic in _accounts) {
            
            AccountEntity *_account = [[AccountEntity alloc] init];
            
            _account._accountId = [[_accountDic valueForKey:RES_ACCOUNTID] intValue];
            _account._accountName = [_accountDic valueForKey:RES_ACCOUNTNAME];
            
            NSDictionary *_userRoleDic = [_accountDic valueForKey:RES_USERROLE];
            
            UserRoleEntity *_userRole = [[UserRoleEntity alloc] init];
            
            _userRole._id = [[_userRoleDic valueForKey:RES_ID] intValue];
            _userRole._name = [_userRoleDic valueForKey:RES_NAME];
            
            NSDictionary *_picPermissionDic = [_userRoleDic valueForKey:RES_PICTUREPERMISSION];
            PermissionEntity *_picPermission = [[PermissionEntity alloc] init];
            
            _picPermission._canAdd = ([[_picPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _picPermission._canEdit = ([[_picPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _picPermission._canView = ([[_picPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _picPermission._canShare = ([[_picPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _picPermission._canDelete = ([[_picPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
            
            _userRole._picturePermissions = _picPermission;
            
            
            NSDictionary *_eventPermissionDic = [_userRoleDic valueForKey:RES_EVENTPERMISSION];
            PermissionEntity *_eventPermission = [[PermissionEntity alloc] init];
            
            _eventPermission._canAdd = ([[_eventPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _eventPermission._canEdit = ([[_eventPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _eventPermission._canView = ([[_eventPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _eventPermission._canShare = ([[_eventPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _eventPermission._canDelete = ([[_eventPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
        
            _userRole._eventPermissions = _eventPermission;
            
            
            NSDictionary *_accountPermissionDic = [_userRoleDic valueForKey:RES_EVENTPERMISSION];
            PermissionEntity *_accountPermission = [[PermissionEntity alloc] init];
            
            _accountPermission._canAdd = ([[_accountPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _accountPermission._canEdit = ([[_accountPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _accountPermission._canView = ([[_accountPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _accountPermission._canShare = ([[_accountPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _accountPermission._canDelete = ([[_accountPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
            
            _userRole._accountPermissions = _eventPermission;
        
            
            NSDictionary *_gamelogPermissionDic = [_userRoleDic valueForKey:RES_EVENTPERMISSION];
            PermissionEntity *_gamelogPermission = [[PermissionEntity alloc] init];
            
            _gamelogPermission._canAdd = ([[_gamelogPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _gamelogPermission._canEdit = ([[_gamelogPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _gamelogPermission._canView = ([[_gamelogPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _gamelogPermission._canShare = ([[_gamelogPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _gamelogPermission._canDelete = ([[_gamelogPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
            
            _userRole._gamelogPermissions = _gamelogPermission;
            
            
            NSDictionary *_mapPermissionDic = [_userRoleDic valueForKey:RES_EVENTPERMISSION];
            PermissionEntity *_mapPermission = [[PermissionEntity alloc] init];
            
            _mapPermission._canAdd = ([[_mapPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _mapPermission._canEdit = ([[_mapPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _mapPermission._canView = ([[_mapPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _mapPermission._canShare = ([[_mapPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _mapPermission._canDelete = ([[_mapPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
            
            _userRole._mapPermissions = _mapPermission;
            
            
            NSDictionary *_chatPermissionDic = [_userRoleDic valueForKey:RES_EVENTPERMISSION];
            PermissionEntity *_chatPermission = [[PermissionEntity alloc] init];
            
            _chatPermission._canAdd = ([[_chatPermissionDic valueForKey:RES_CANADD] intValue] == 1);
            _chatPermission._canEdit = ([[_chatPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
            _chatPermission._canView = ([[_chatPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
            _chatPermission._canShare = ([[_chatPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
            _chatPermission._canDelete = ([[_chatPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
            
            _userRole._chatPermissions = _chatPermission;
            
            _account._userRole = _userRole;
            
            
            // account profilePicture
            NSDictionary *_profilePictureDic = [_accountDic objectForKey:RES_PROFILE_PIC];
            
            ProfilePictureEntity *_accProfilePicture = [[ProfilePictureEntity alloc] init];
            
            if(profileDic != [NSNull null]) {
                
                _accProfilePicture._pictureId = [_profilePictureDic valueForKey:RES_PICTUREID];
                _accProfilePicture._size = [[_profilePictureDic valueForKey:RES_SIZE] intValue];
                _accProfilePicture._width = [[_profilePictureDic valueForKey:RES_WIDTH] intValue];
                _accProfilePicture._height = [[_profilePictureDic valueForKey:RES_HEIGHT] intValue];
                _accProfilePicture._blobUrl = [_profilePictureDic valueForKey:RES_BLOUURL];
                
                
                NSMutableArray *_accThumbnails = [_profilePictureDic valueForKey:RES_THUMNAILS];
                
                for (NSDictionary *_accThumbnailsDic in _accThumbnails) {
                
                    ThumbnailEntity *_accThumbnail = [[ThumbnailEntity alloc] init];
                
                    _accThumbnail._thumbnailId = [[_accThumbnailsDic valueForKey:RES_THUMNAILID] intValue];
                    _accThumbnail._blobUrl = [_accThumbnailsDic valueForKey:RES_BLOUURL];
                    _accThumbnail._width = [[_accThumbnailsDic valueForKey:RES_WIDTH] intValue];
                    _accThumbnail._height = [[_accThumbnailsDic valueForKey:RES_HEIGHT] intValue];
                    _accThumbnail._size = [[_accThumbnailsDic valueForKey:RES_SIZE] intValue];
                    
                    [_accProfilePicture._thumbnails addObject:_accThumbnail];
                }
            }
            _account._profilePicture = _accProfilePicture;
            
            _account._memberStat = [[_accountDic valueForKey:RES_MEMBERSTAT] intValue];
            _account._propertyStat = [[_accountDic valueForKey:RES_PROPERTYSTAT] intValue];
            _account._dataUsed = [[_accountDic valueForKey:RES_DATAUSED] intValue];
            
            
            NSMutableArray *_accProperties = [_accountDic valueForKey:RES_PROPERTIES];
            
            for (NSDictionary *_propertyDic in _accProperties) {
                
                PropertyEntity *_accProperty = [[PropertyEntity alloc] init];
                
                // for map reservation management by property
                _user._propertyId = [[_propertyDic valueForKey:RES_PROPERTYID] intValue];
                
                [CommonUtils saveUserInfo];
                
                _accProperty._propertyId = [[_propertyDic valueForKey:RES_PROPERTYID] intValue];
                _accProperty._propertyName = [_propertyDic valueForKey:RES_PROPERTYNAME];
                _accProperty._zipCode = [_propertyDic valueForKey:RES_ZIPCODE];
                _accProperty._dataUsed = [[_propertyDic valueForKey:RES_DATAUSED] intValue];
                
                [_account._properties addObject:_accProperty];
            }
            
            [_user._accounts addObject:_account];
        }
        
        NSDictionary *_organizationDic = [responseObject objectForKey:RES_ORGANIZATION];
        
        OrganizationEntity *_organization = [[OrganizationEntity alloc] init];
        
        _organization._organizationName = [_organizationDic valueForKey:RES_ORGANIZATIONNAME];
        _organization._organizationId = [[_organizationDic valueForKey:RES_ORGANIZATIONID] intValue];
        _organization._organizationPictureUrl = [_organizationDic valueForKey:RES_ORGANIZATIONPICURL];
        
        _user._organization = _organization;
        
        NSDictionary *_widgetDic = [responseObject valueForKey:RES_WEBWIDGETS];
        
        WidgetEntity *_widget = [[WidgetEntity alloc] init];
        
        _widget._widget = [_widgetDic valueForKey:RES_WIDGETS];
        
        _user._webWidgets = _widget;
        
        [self gotoMain];
        
//        if(nResultCode == CODE_SUCCESS) {
//            
//            [[JLToast makeText:@"New customer was added successfully" duration:2] show];
//            
//        } else if (nResultCode == CODE_USERNAME_EXIST){
//            
//            [self showAlertDialog:nil message:EXIST_CUS_NAME positive:ALERT_OK negative:nil];
//            
//        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}
- (IBAction)fbLoginAction:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
         }
     }];
}


- (void) gotoMain {
    
    UITabBarController * mainVC = (UITabBarController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabController"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:mainVC];
    
}


@end
