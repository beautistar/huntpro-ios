//
//  MyPicViewController.m
//  HuntPro
//
//

#import "MyPicViewController.h"
#import "PictureDetailViewController.h"

#import "PictureCell.h"
#import "CommonUtils.h"
//#import "UIViewMYPop.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "PEARImageSlideViewController.h"
#import "LocationEntity.h"
#import "TagEntity.h"
#import "LoadingCollectionViewCell.h"


@interface MyPicViewController() <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    NSString *photoPath;
    
    NSMutableArray * _tagList;
    NSMutableArray * _locationList;
    
    NSMutableArray *_tagDropList;
    NSMutableArray *_locationDropList;
    
    NSMutableArray *_selectedLocations;
    NSMutableArray *_selectedTags;
    
    NSMutableArray *_pictureList;
    NSMutableArray * _pictureArr;

    
    
    NSArray *_sortTypes, *_sortParams;
    int _startWith;
    
    
    UserEntity *_user;
    LocationEntity * _selectedLocation;
    TagEntity * _selectedTag;
    
    int _selectedLocationId;
    int _selectedTagId;
    
    BOOL _isLoadingGallery;
    BOOL _isLoadedAllGallery;
    
}


@property (weak, nonatomic) IBOutlet UICollectionView *cvPicList;
@property (weak, nonatomic) IBOutlet UIButton *btnTag;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnLocation;
@property (weak, nonatomic) IBOutlet UITextField *tfSortType;
@property (weak, nonatomic) IBOutlet UITextField *tfSortParams;
@property (weak, nonatomic) IBOutlet UITextField *tfStartWith;
@property (weak, nonatomic) IBOutlet UIButton *btnSortType;
@property (weak, nonatomic) IBOutlet UIButton *btnSortParam;

@property (nonatomic,retain)PEARImageSlideViewController * slideImageViewController;

@end

@implementation MyPicViewController

- (void) viewDidLoad {
    
    _user = APPDELEGATE.Me;
    
    _locationList = [[NSMutableArray alloc] init];
    _tagList = [[NSMutableArray alloc] init];
    
    _locationDropList = [[NSMutableArray alloc] init];
    _tagDropList = [[NSMutableArray alloc] init];
    
    _pictureArr = [[NSMutableArray alloc] init];
    
    _sortTypes = [[NSArray alloc] initWithObjects:@"Created", @"Modified", @"Taken", nil];
    
    _sortParams = [[NSArray alloc] initWithObjects:@"Ascending", @"Descending", nil];
    
    _isLoadingGallery = false;
    _isLoadedAllGallery = false;
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    self.tabBarController.tabBar.hidden = NO;

    _selectedLocations = [[NSMutableArray alloc] init];
    _selectedTags = [[NSMutableArray alloc] init];
    _pictureList = [[NSMutableArray alloc] init];
    
    self.lblLocation.text = @"Locations";
    self.lblTag.text = @"Tags";
    
    [_selectedTags removeAllObjects];
    [_selectedLocations removeAllObjects];
    
    
    [self.navigationController setNavigationBarHidden:NO];
    
    if (PROPERTYID == 0 || ACCOUNTID == 0) {
        
        AccountEntity * account0 = _user._accounts[0];
        
        ACCOUNTID = account0._accountId;
        PropertyEntity * property0 = account0._properties[0];
        
        PROPERTYID = property0._propertyId;
    }
    
//    if (PICTURE_BACK == 0) {
    
    [_pictureList removeAllObjects];
    [_pictureArr removeAllObjects];

//    [self.cvPicList reloadData];
    
        [self getTags];
//    }
    
}

- (IBAction)cameraAction:(id)sender {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (IBAction)plusAction:(id)sender {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

- (IBAction)locationAction:(id)sender {
    
    //[self showPopView];
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Tag" rows:_locationDropList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        NSString *value ;
        if (_selectedLocations.count != 0) {
            value = [NSString stringWithFormat:@"%@%@%@", _lblLocation.text, @", ", selectedValue];
            
        } else {
            value = selectedValue;
        }
        
        [self.lblLocation setText:value];
        _selectedLocation = _locationList[selectedIndex];
        
        [_selectedLocations addObject:_selectedLocation];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        _lblLocation.text = @"";
        [_selectedLocations removeAllObjects];
        
    } origin:self.btnLocation];

}

- (void) showPopView {
    
//    [self.popView setHidden:NO];
//    
//    self.popView.transform = CGAffineTransformMakeScale(1.3, 1.3);
//    self.popView.alpha = 0;
//    [UIView animateWithDuration:0.3f animations:^{
//        self.popView.alpha = 1;
//        self.popView.transform = CGAffineTransformMakeScale(1, 1);
//    }];
}

- (void) hidePopView {
    
//    [UIView animateWithDuration:0.3f animations:^{
//        self.popView.alpha = 0;
//    }];
}

- (IBAction)tagAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Tag" rows:_tagDropList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        NSString *value ;
        if (_selectedTags.count != 0) {
            value = [NSString stringWithFormat:@"%@%@%@", _lblTag.text, @", ", selectedValue];
            
        } else {
            value = selectedValue;
        }
        
        [self.lblTag setText:value];
        _selectedTag = _tagList[selectedIndex];
        
        [_selectedTags addObject:_selectedTag];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
        _lblTag.text = @"";
        [_selectedTags removeAllObjects];
        
    } origin:self.btnTag];
}

- (IBAction)sortByTypeAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Tag" rows:_sortTypes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.tfSortType setText:selectedValue];
        
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnSortType];
    
}

- (IBAction)sortByParameterAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Tag" rows:_sortParams initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.tfSortParams setText:selectedValue];
        
        [self getPicture];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnSortParam];
}

- (IBAction)settingAction:(id)sender {
    
    [self openSettingVC];
}


//#pragma mark - 
#pragma mark - get Location API

- (void) getLocation {///api/Location/ByProperty/{propertyId}
    
    [_locationDropList removeAllObjects];
    [_locationList removeAllObjects];
    
    [self showLoadingView];
    
//    //for test
//    int propertyId = 111;
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_LOCATION, PROPERTYID];
    
    NSLog(@"getLocation url : %@", url);
    
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"get location by property result : %@", responseObject);
        
        NSArray *_locations = (NSArray *) responseObject;
        
        
        for (NSDictionary * _dict in _locations) {
            
            LocationEntity * _location = [[LocationEntity alloc] init];
            
            _location._id = [[_dict valueForKey:RES_ID] intValue];
            _location._latitude = [[_dict valueForKey:RES_LATITUDE] floatValue];
            _location._longitude = [[_dict valueForKey:RES_LONGITUDE] floatValue];
            _location._title = [_dict valueForKey:RES_TITLE];
            _location._ownerId = [_dict valueForKey:RES_OWNER_ID];
            _location._propertyId = [[_dict valueForKey:RES_PROPERTYID] intValue];
            _location._description = [_dict valueForKey:RES_DETAILEDACTION];
            
            [_locationDropList addObject:_location._title];
            
            [_locationList addObject:_location];
        }
        
        [self getPicture];
    
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"You are not authenticated in this time" positive:ALERT_OK negative:nil];
        
    }];
}

#pragma mark - get Tag API

- (void) getTags {///api/Tags/ByAccount/{accountId}
    
    [_tagList removeAllObjects];
    [_tagDropList removeAllObjects];
    
    [self showLoadingView];
    //for test
    //int accountId = 4;
    
    NSString *url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GET_TAG, ACCOUNTID];
    
    NSLog(@"getTags url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"get tag by account : %@", responseObject);
        
        NSArray *_locations = (NSArray *) responseObject;
        
        
        for (NSDictionary * _dict in _locations) {
            
            TagEntity *_tag = [[TagEntity alloc] init];
            
            _tag._accountId = [[_dict valueForKey:RES_ACCOUNTID] intValue];
            _tag._title = [_dict valueForKey:RES_TITLE];
            _tag._description = [_dict valueForKey:RES_DESCRIPTION];
            _tag._userAccount = [[_dict valueForKey:RES_USERCOUNT] intValue];
            
            [_tagDropList addObject:_tag._title];
            
            [_tagList addObject:_tag];
            
        }
        
        [self getLocation];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"You are not authenticated in this time" positive:ALERT_OK negative:nil];
        
    }];
    
}

- (IBAction)refreshAction:(id)sender {
    
    [self showLoadingView];
    
    [self getPicture];
}

- (void) getPicture {
    [_pictureList removeAllObjects];
    [_pictureArr removeAllObjects];
    [self getPictureFrom:0];
    //[self.cvPicList reloadData];
}

- (void) getPictureFrom:(NSInteger)start {//POST /api/Gallery/Property/{sortType}/{sortParameter}/{propertyId}/{numResults}/{startWith}
    
    if (_isLoadingGallery) {
        return;
    }
    
    _isLoadingGallery = true;
    
//    [self showLoadingViewWithTitle:nil];
    
    // for test
    //int propertyId = 111;
    int numResult = 15;
    
//    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@/%d/%d/%d", SERVER_URL, REQ_GET_GALLERY, _tfSortType.text, _tfSortParams.text, PROPERTYID, numResult, [_tfStartWith.text intValue]];
    NSString *url = [NSString stringWithFormat:@"%@%@/%@/%@/%d/%d/%d", SERVER_URL, REQ_GET_GALLERY, @"Created", @"Descending", PROPERTYID, numResult, (int)start];

    
    NSLog(@"/api/Gallery/Property url %@", url);
    
    NSMutableArray * locArray = [[NSMutableArray alloc] init];
    for (LocationEntity * _location in _selectedLocations) {
        [locArray addObject: @{PARAM_ID : [NSNumber numberWithInt: _location._id]}];
    }
    
    NSMutableArray *tagArray = [[NSMutableArray alloc] init];
    for (TagEntity *tag in _selectedTags) {
        [tagArray addObject:@{PARAM_TITLE: tag._title, PARAM_ACCOUNTID: [NSNumber numberWithInt:tag._accountId]}];
    }
    
    NSDictionary *params = @{
                             PARAM_TAGS : tagArray,
                             PARAM_LOCATIONS : locArray,
                             };
    
    NSLog(@"dictionary: %@", params);
    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        _isLoadingGallery = false;
        
        [self hideLoadingView];
        
        NSLog(@"gallery respond : %@", responseObject);
        
        // success
        
        NSArray *pictures = (NSArray *) responseObject;
        
        if (pictures.count > 0) {        
        
            for (NSDictionary * _dict in pictures) {
                
                ProfilePictureEntity *_picture = [[ProfilePictureEntity alloc] init];
                
                _picture._pictureId = [_dict valueForKey:RES_PICTUREID];
                _picture._size = [[_dict valueForKey:RES_SIZE] intValue];
                _picture._width = [[_dict valueForKey:RES_WIDTH] intValue];
                _picture._height = [[_dict valueForKey:RES_HEIGHT] intValue];
                _picture._blobUrl = [_dict valueForKey:RES_BLOUURL];
                
                id thumbnailDic = [_dict valueForKey:RES_THUMNAILS];
                
                if (thumbnailDic != [NSNull null]) {
                    
                    for (NSDictionary *_thumbnailDic in thumbnailDic) {
                    
                        ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
                        
                        //NSDictionary *_thumbnailDic = (NSDictionary *) thumbnailDic;
                        
                        _thumbnail._thumbnailId = [[_thumbnailDic valueForKey:RES_THUMNAILID] intValue];
                        _thumbnail._blobUrl = [_thumbnailDic valueForKey:RES_BLOUURL];
                        _thumbnail._width = [[_thumbnailDic valueForKey:RES_WIDTH] intValue];
                        _thumbnail._height = [[_thumbnailDic valueForKey:RES_HEIGHT] intValue];
                        _thumbnail._size = [[_thumbnailDic valueForKey:RES_SIZE] intValue];
                        
                        [_picture._thumbnails addObject:_thumbnail];
                    }
                }
                
                [_pictureList addObject:_picture];
                
                [_pictureArr addObject:_picture._blobUrl];
                
            }
        }
        if (pictures.count < numResult) {
            _isLoadedAllGallery = true;
        }
        
        //[self.cvPicList reloadData];
        
        [_cvPicList reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        _isLoadingGallery = false;
        [self hideLoadingView];
        NSLog(@"error : %@", error);
        
        [self.cvPicList reloadData];
    }];
     
}


#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                //[_imvTemp setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - collection view delegate

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (_isLoadingGallery || _isLoadedAllGallery == false) {
        return _pictureList.count + 1;
    } else if (_pictureList.count == 0 && _isLoadedAllGallery == true) {
        return 1;
    } else {
        return _pictureList.count;
    }
//    return 5;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat cellWidth = (screenSize.width - 40) / 3 - 10;
    CGSize cellSize = CGSizeMake(cellWidth, cellWidth);
    return cellSize;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row >= _pictureList.count) {
        LoadingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:LoadingCollectionViewCellIdentifier forIndexPath:indexPath];
        if (_isLoadingGallery || _isLoadedAllGallery == false) {
            [cell showLoadingIndicator];
            [self getPictureFrom:_pictureList.count];
        } else if (_pictureList.count == 0 && _isLoadedAllGallery == true) {
            [cell showEmptyMessage];
        } else {
            
        }
        return cell;
    } else {
        PictureCell *cell = (PictureCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"PictureCell" forIndexPath:indexPath];
        
        [cell setImage:_pictureArr[indexPath.row]];
        return cell;
    }
}



- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PictureDetailViewController *picDetailVC = (PictureDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"PictureDetailViewController"];
    
    NSLog(@"index:%d", (int)indexPath.row);
    picDetailVC._selectedPicture = _pictureList[indexPath.row];
    [self.navigationController pushViewController:picDetailVC animated:YES];
    
//    _slideImageViewController = [PEARImageSlideViewController new];
//    
//    NSArray *imageLists = @[
//                            [UIImage imageNamed:@"180.png"],
//                            [UIImage imageNamed:@"180.png"],
//                            [UIImage imageNamed:@"180.png"],
//                            [UIImage imageNamed:@"180.png"],
//                            [UIImage imageNamed:@"180.png"]
//                            ].copy;
//    [_slideImageViewController setImageLists:imageLists];
//    [_slideImageViewController showAtIndex:0];
}

#pragma mark - view touch action

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [self hidePopView];
}

#pragma mark - unwind segue

- (IBAction) prepareForUnwindToMyPic:(UIStoryboardSegue *)segue {
    
    if ([segue.identifier isEqualToString: @"unwind2MyPic"]) {
        
        PictureDetailViewController *sourceVC = (PictureDetailViewController *)[segue sourceViewController];
        
        _pictureList = sourceVC._picList;
        
        //[_cvPicList reloadData];
        
    }
}


@end
