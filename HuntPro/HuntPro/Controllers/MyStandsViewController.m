//
//  MyStandsViewController.m
//  HuntPro
//
//

#import "MyStandsViewController.h"
#import "MyStandsCell.h"
//#import "UIViewMYPop.h"
#import "CommonUtils.h"
#import "StandEntity.h"
@import JLToast;

@interface MyStandsViewController() <UITableViewDelegate, UITableViewDataSource> {
    
    UserEntity *_user;
    NSMutableArray *_myStandList;

}
@property (weak, nonatomic) IBOutlet UITableView *tblStands;

@property (weak, nonatomic) IBOutlet UIView *vAddStand;

@end


@implementation MyStandsViewController


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _user = APPDELEGATE.Me;
    
    _myStandList = [[NSMutableArray alloc] init];
    
    if (PROPERTYID == 0 || ACCOUNTID == 0) {
        
        AccountEntity * account0 = _user._accounts[0];
        
        ACCOUNTID = account0._accountId;
        PropertyEntity * property0 = account0._properties[0];
        
        PROPERTYID = property0._propertyId;
    }
    
    //[self getMyStands];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self getMyStands];
}

- (IBAction)addStandAction:(id)sender {
    
    [self showPopView];
    
}

- (IBAction)cancelAction:(id)sender {
    
    [self hidePopView];
}

- (IBAction)okAction:(id)sender {
    
    
    [self hidePopView];
}

- (IBAction)settingAction:(id)sender {
    
    [self openSettingVC];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
   
//    [self hidePopView];
}

- (void) showPopView {
    
    [self.vAddStand setHidden:NO];
    
    self.vAddStand.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.vAddStand.alpha = 0;
    [UIView animateWithDuration:0.3f animations:^{
        self.vAddStand.alpha = 1;
        self.vAddStand.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}

- (void) hidePopView {

    [UIView animateWithDuration:0.3f animations:^{
        self.vAddStand.alpha = 0;
    }];
}

#pragma mark - 
#pragma mark - get my stands

- (void) getMyStands {
    
    [_myStandList removeAllObjects];
    
    NSLog(@"User propoery id %d", _user._propertyId);
    
    [self showLoadingView];
    
//    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@/%d", SERVER_URL, REQ_GET_STANDS, _user._propertyId, PARAM_TYPE, 1];
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@/%d", SERVER_URL, REQ_GET_STANDS, PROPERTYID, PARAM_TYPE, 1];
    
    NSLog(@"get mystands request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"getStand response : %@", responseObject);
        
        [self hideLoadingView];
        
        NSArray *_myStands = (NSArray *) responseObject;
        
        for (NSDictionary * _dict in _myStands) {
            
            StandEntity *_stand = [[StandEntity alloc] init];
            
            _stand._id = [[_dict valueForKey:RES_ID] intValue];
            _stand._title = [_dict valueForKey:RES_TITLE];
            
            NSArray *_locationPointTypes = [_dict valueForKey:RES_LOCATIONPOINTTYPES];
            
            for (NSDictionary *_locationPointTypeDic in _locationPointTypes) {
            
                LocationPointTypeEntity *_locPointType = [[LocationPointTypeEntity alloc] init];
                
                _locPointType._id = [[_locationPointTypeDic valueForKey:RES_ID] intValue];
                _locPointType._name = [_locationPointTypeDic valueForKey:RES_NAME];
                
                [_stand._locationpointTypes addObject:_locPointType];
            }
            
            id reservationDic = [_dict objectForKey:RES_RESERVATION];
            
            ReservationEntity *_reservation = [[ReservationEntity alloc] init];
            
            if (reservationDic != [NSNull null]) {
                
                NSDictionary *_reservationDic = (NSDictionary *) reservationDic;
                
                //if ([reservationDic valueForKey:RES_ID] != nil) {
                _reservation._id = [_reservationDic valueForKey:RES_ID];
                //}
                _reservation._locationId = [[_reservationDic valueForKey:RES_LOCATIONID] intValue];
                
                NSDictionary *_standUserDic = [_reservationDic objectForKey:RES_USER];
                
                StandUserEntity *_standUser = [[StandUserEntity alloc] init];
                _standUser._id = [_standUserDic valueForKey:RES_ID] ;
                _standUser._name = [_standUserDic valueForKey:RES_NAME];
                
                id _profilePictureDic = [_standUserDic objectForKey:RES_PROFILE_PIC];
                ProfilePictureEntity *_profilePicture = [[ProfilePictureEntity alloc] init];
                
                if(_profilePictureDic != [NSNull null]) {
                    
                    NSDictionary * _profileDic = (NSDictionary *) _profilePictureDic;
                    
                    _profilePicture._pictureId = [_profileDic valueForKey:RES_PICTUREID];
                    _profilePicture._size = [[_profileDic valueForKey:RES_SIZE] intValue];
                    _profilePicture._width = [[_profileDic valueForKey:RES_WIDTH] intValue];
                    _profilePicture._height = [[_profileDic valueForKey:RES_HEIGHT] intValue];
                    _profilePicture._blobUrl = [_profileDic valueForKey:RES_BLOUURL];
                    
                    
                    NSMutableArray *_thumbnails = [_profileDic valueForKey:RES_THUMNAILS];
                    
                    for (NSDictionary *_thDict in _thumbnails) {
                        
                        ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
                        
                        _thumbnail._thumbnailId = [[_thDict valueForKey:RES_THUMNAILID] intValue];
                        _thumbnail._blobUrl = [_thDict valueForKey:RES_BLOUURL];
                        _thumbnail._width = [[_thDict valueForKey:RES_WIDTH] intValue];
                        _thumbnail._height = [[_thDict valueForKey:RES_HEIGHT] intValue];
                        _thumbnail._size = [[_thDict valueForKey:RES_SIZE] intValue];
                        
                        [_profilePicture._thumbnails addObject:_thumbnail];
                    }
                }
                
                _standUser._profilePicture = _profilePicture;
                
                _reservation._standUser = _standUser;
                
                _reservation._startTime = [_reservationDic valueForKey:RES_STARTTIME];
                _reservation._endTime = [_reservationDic valueForKey:RES_ENDTIME];
                
            }
            _stand._reservation = _reservation;
            
            [_myStandList addObject:_stand];
            
        }
        
        [self.tblStands reloadData];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@", error);
        
//        NSLog(@"%@", error)
        
//        NSString *errorRes = [[NSString alloc] initWithData:(NSData *)error encoding:NSUTF8StringEncoding];
//        
//        NSLog(@"%@", [errorRes valueForKey:RES_MESSAGE]);
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"You are not authorized to get this contect" positive:ALERT_OK negative:nil];
        
    }];
}

#pragma mark - to check in & out

- (void) checkOut : (int) _locId {
    
    [self showLoadingView];
    
//    StandEntity *_selStand = _myStandList[1];
    //int _locId =_selStand._reservation._locationId;
    //_locId = 824;
    ////////////working/////////////////
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_CHECKOUT_STAND, _locId];
    
    NSLog(@"checkin request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"checkin response : %@", responseObject);
        
        ReservationEntity *_reservation = [[ReservationEntity alloc] init];
        
        _reservation._id = [responseObject valueForKey:RES_ID];
        _reservation._locationId = [[responseObject valueForKey:RES_LOCATIONID] intValue];
        
        id standUserDic = [responseObject objectForKey:RES_USER];
        
        StandUserEntity *_standUser = [[StandUserEntity alloc] init];
        
        if (standUserDic != [NSNull null]) {
            
            NSDictionary *_standUserDic = (NSDictionary *) standUserDic;
        
            _standUser._id = [_standUserDic valueForKey:RES_ID] ;
            _standUser._name = [_standUserDic valueForKey:RES_NAME];
            
            id _profilePictureDic = [_standUserDic objectForKey:RES_PROFILE_PIC];
            ProfilePictureEntity *_profilePicture = [[ProfilePictureEntity alloc] init];
            
            if(_profilePictureDic != [NSNull null]) {
                
                NSDictionary * _profileDic = (NSDictionary *) _profilePictureDic;
                
                _profilePicture._pictureId = [_profileDic valueForKey:RES_PICTUREID];
                _profilePicture._size = [[_profileDic valueForKey:RES_SIZE] intValue];
                _profilePicture._width = [[_profileDic valueForKey:RES_WIDTH] intValue];
                _profilePicture._height = [[_profileDic valueForKey:RES_HEIGHT] intValue];
                _profilePicture._blobUrl = [_profileDic valueForKey:RES_BLOUURL];
                
                
                NSMutableArray *_thumbnails = [_profileDic valueForKey:RES_THUMNAILS];
                
                for (NSDictionary *_thDict in _thumbnails) {
                    
                    ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
                    
                    _thumbnail._thumbnailId = [[_thDict valueForKey:RES_THUMNAILID] intValue];
                    _thumbnail._blobUrl = [_thDict valueForKey:RES_BLOUURL];
                    _thumbnail._width = [[_thDict valueForKey:RES_WIDTH] intValue];
                    _thumbnail._height = [[_thDict valueForKey:RES_HEIGHT] intValue];
                    _thumbnail._size = [[_thDict valueForKey:RES_SIZE] intValue];
                    
                    [_profilePicture._thumbnails addObject:_thumbnail];
                }
            }
            _standUser._profilePicture = _profilePicture;
        }
        _reservation._standUser = _standUser;
        _reservation._startTime = [responseObject valueForKey:RES_STARTTIME];
        _reservation._endTime = [responseObject valueForKey:RES_ENDTIME];
        
//        [Toast makeText:@"check out success" duration: 3];
        [[JLToast makeText:@"check out success"] show];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@", error);
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"An unknown error has occured" positive:ALERT_OK negative:nil];
        
    }];   
    
}

- (void) checkIn : (int) _locId {
    
    [self showLoadingView];
    
    //    StandEntity *_selStand = _myStandList[1];
    //int _locId =_selStand._reservation._locationId;
    //_locId = 824;
    ////////////working/////////////////
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_CHECKOUT_STAND, _locId];
    
    NSLog(@"checkin request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"checkin response : %@", responseObject);
        
        ReservationEntity *_reservation = [[ReservationEntity alloc] init];
        
        _reservation._id = [responseObject valueForKey:RES_ID];
        _reservation._locationId = [[responseObject valueForKey:RES_LOCATIONID] intValue];
        
        id standUserDic = [responseObject objectForKey:RES_USER];
        
        StandUserEntity *_standUser = [[StandUserEntity alloc] init];
        
        if (standUserDic != [NSNull null]) {
            
            NSDictionary *_standUserDic = (NSDictionary *) standUserDic;
            
            _standUser._id = [_standUserDic valueForKey:RES_ID] ;
            _standUser._name = [_standUserDic valueForKey:RES_NAME];
            
            id _profilePictureDic = [_standUserDic objectForKey:RES_PROFILE_PIC];
            ProfilePictureEntity *_profilePicture = [[ProfilePictureEntity alloc] init];
            
            if(_profilePictureDic != [NSNull null]) {
                
                NSDictionary * _profileDic = (NSDictionary *) _profilePictureDic;
                
                _profilePicture._pictureId = [_profileDic valueForKey:RES_PICTUREID];
                _profilePicture._size = [[_profileDic valueForKey:RES_SIZE] intValue];
                _profilePicture._width = [[_profileDic valueForKey:RES_WIDTH] intValue];
                _profilePicture._height = [[_profileDic valueForKey:RES_HEIGHT] intValue];
                _profilePicture._blobUrl = [_profileDic valueForKey:RES_BLOUURL];
                
                
                NSMutableArray *_thumbnails = [_profileDic valueForKey:RES_THUMNAILS];
                
                for (NSDictionary *_thDict in _thumbnails) {
                    
                    ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
                    
                    _thumbnail._thumbnailId = [[_thDict valueForKey:RES_THUMNAILID] intValue];
                    _thumbnail._blobUrl = [_thDict valueForKey:RES_BLOUURL];
                    _thumbnail._width = [[_thDict valueForKey:RES_WIDTH] intValue];
                    _thumbnail._height = [[_thDict valueForKey:RES_HEIGHT] intValue];
                    _thumbnail._size = [[_thDict valueForKey:RES_SIZE] intValue];
                    
                    [_profilePicture._thumbnails addObject:_thumbnail];
                }
            }
            _standUser._profilePicture = _profilePicture;
        }
        _reservation._standUser = _standUser;
        _reservation._startTime = [responseObject valueForKey:RES_STARTTIME];
        _reservation._endTime = [responseObject valueForKey:RES_ENDTIME];
        
//        [Toast makeText:@"check in success" duration: 3];
        [[JLToast makeText:@"check in success"] show];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@", error);
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"An unknown error has occured" positive:ALERT_OK negative:nil];
        
    }];
    
}

#pragma mark -
#pragma mark - tableview delegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _myStandList.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyStandsCell *cell = (MyStandsCell *) [tableView dequeueReusableCellWithIdentifier:@"MyStandsCell"];
    
    StandEntity *selectedStand = _myStandList[(int) indexPath.row];
    
    [cell setStand:selectedStand];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    StandEntity *_selStand = _myStandList[(int) indexPath.row];
    
    if (_selStand._reservation._standUser._name.length == 0) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:ALERT_CHECKIN
                                     message:ALERT_CHECKIN_MSG
                                     preferredStyle:UIAlertControllerStyleAlert];
        

        UIAlertAction * yesButton = [UIAlertAction
                                         actionWithTitle:ALERT_OK
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Handel your yes please button action here
                                             [self checkIn:_selStand._id];
                                         }];
        [alert addAction:yesButton];

        
        UIAlertAction * noButton = [UIAlertAction
                                        actionWithTitle:ALERT_CANCEL
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                        }];
            
       [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    if ([_selStand._reservation._standUser._id isEqual:_user._id]) {
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:ALERT_CHECKOUT
                                     message:ALERT_CHECKOUT_MSG
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:ALERT_OK
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [self checkOut:_selStand._id];
                                     }];
        [alert addAction:yesButton];
        
        
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:ALERT_CANCEL
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];

        
    }

}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [self.vAddStand endEditing:YES];
}

@end
