//
//  ActivityViewController.m
//  HuntPro
//
//

#import "ActivityViewController.h"
#import "FSLineChart.h"

#import "UIColor+FSPalette.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>


@interface ActivityViewController() {
    
    NSMutableArray *tagList;
    
}

@property (weak, nonatomic) IBOutlet FSLineChart *hourlyActView;
@property (weak, nonatomic) IBOutlet FSLineChart *dailyActView;
@property (weak, nonatomic) IBOutlet UIView *popView;
@property (weak, nonatomic) IBOutlet UIButton *btnTag;
@property (weak, nonatomic) IBOutlet UILabel *lblTag;

@end

@implementation ActivityViewController

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    tagList = [[NSMutableArray alloc] init];
    
    tagList = [NSMutableArray arrayWithObjects:@"first", @"second", @"third", nil];
    

    
    //[self setHourlyView];
    
    //[self setChart];
    //[self loadChartWithDates];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self setHourlyChart];
    
    [self setDailyChart];
}

- (IBAction)locationAction:(id)sender {
    
    [self showPopView];
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self hidePopView];    
}

- (void) showPopView {
    
    [self.popView setHidden:NO];
    
    self.popView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.popView.alpha = 0;
    [UIView animateWithDuration:0.3f animations:^{
        self.popView.alpha = 1;
        self.popView.transform = CGAffineTransformMakeScale(1, 1);
    }];
    
}

- (void) hidePopView {
    
    [UIView animateWithDuration:0.3f animations:^{
        self.popView.alpha = 0;
    }];
}


- (IBAction)tagAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Tag" rows:tagList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [self.lblTag setText:selectedValue];
        //self._selectedCustomer = CUSTOMER_LIST[selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:self.btnTag];
}

- (IBAction)settingAction:(id)sender {

    [self openSettingVC];
}

- (void) setHourlyChart {
    
    NSMutableArray* chartData = [NSMutableArray arrayWithCapacity:15];
    
    for(int i=0;i<15;i++) {
        int r = abs(rand() % 14);
        chartData[i] = [NSNumber numberWithInt:r];
    }
    
    // Setting up the line chart
    _hourlyActView.verticalGridStep = 7;
    _hourlyActView.horizontalGridStep = 14;
    
    _hourlyActView.fillColor = [UIColor colorWithRed:135/255.0f green:174/255.0f blue:98/255.0f alpha:0.7f];
    _hourlyActView.color = [UIColor colorWithRed:135/255.0f green:174/255.0f blue:98/255.0f alpha:1.0f];;
    _hourlyActView.valueLabelPosition = ValueLabelLeftMirrored - 100;
    
    _hourlyActView.labelForIndex = ^(NSUInteger item) {
        
        NSLog(@"item : %d", (int)item);
        return [NSString stringWithFormat:@"%lu",(unsigned long)item];
    };
    
    _hourlyActView.labelForValue = ^(CGFloat value) {
        return [NSString stringWithFormat:@"%d", (int)value];
    };
    
    [_hourlyActView setChartData:chartData];
}

- (void) setDailyChart {
    
    NSMutableArray* chartData = [NSMutableArray arrayWithCapacity:15];
    
    for(int i=0;i<15;i++) {
        int r = abs(rand() % 14);
        chartData[i] = [NSNumber numberWithInt:r];
    }
    
    // Setting up the line chart
    _dailyActView.verticalGridStep = 7;
    _dailyActView.horizontalGridStep = 14;
    
    _dailyActView.fillColor = [UIColor colorWithRed:135/255.0f green:174/255.0f blue:98/255.0f alpha:0.7f];
    _dailyActView.color = [UIColor colorWithRed:135/255.0f green:174/255.0f blue:98/255.0f alpha:1.0f];;
    _dailyActView.valueLabelPosition = ValueLabelLeftMirrored - 100;
    
    _dailyActView.labelForIndex = ^(NSUInteger item) {
        
        NSLog(@"item : %d", (int)item);
        return [NSString stringWithFormat:@"%lu",(unsigned long)item];
    };
    
    _dailyActView.labelForValue = ^(CGFloat value) {
        return [NSString stringWithFormat:@"%d", (int)value];
    };
    
    [_dailyActView setChartData:chartData];
}



- (void) setHourlyView {
    
    // Generating some dummy data
    NSMutableArray* chartData = [NSMutableArray arrayWithCapacity:7];
    for(int i=0;i<7;i++) {
        chartData[i] = [NSNumber numberWithFloat: (float)i / 30.0f + (float)(rand() % 100) / 500.0f];
    }
    
    NSArray* months = @[@"January", @"February", @"March", @"April", @"May", @"June", @"July"];
    
    // Setting up the line chart
    _hourlyActView.verticalGridStep = 7;
    _hourlyActView.horizontalGridStep = 12;
    _hourlyActView.fillColor = [UIColor orangeColor];
    _hourlyActView.displayDataPoint = YES;
    _hourlyActView.dataPointColor = [UIColor fsOrange];
    _hourlyActView.dataPointBackgroundColor = [UIColor fsOrange];
    _hourlyActView.dataPointRadius = 2;
    _hourlyActView.color = [_hourlyActView.dataPointColor colorWithAlphaComponent:0.3];
    _hourlyActView.valueLabelPosition = ValueLabelLeftMirrored;
    
    _hourlyActView.labelForIndex = ^(NSUInteger item) {
        return months[item];
    };
    
    _hourlyActView.labelForValue = ^(CGFloat value) {
        return [NSString stringWithFormat:@"%.02f €", value];
    };
    
    [_hourlyActView setChartData:chartData];
    
    
}


- (void)loadChartWithDates {
    // Generating some dummy data
    NSMutableArray* chartData = [NSMutableArray arrayWithCapacity:7];
    for(int i=0;i<7;i++) {
        chartData[i] = [NSNumber numberWithFloat: (float)i / 30.0f + (float)(rand() % 100) / 500.0f];
    }
    
    NSArray* months = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7"];
    
    // Setting up the line chart
    _dailyActView.verticalGridStep = 7;
    _dailyActView.horizontalGridStep = 15;
    _dailyActView.fillColor = [UIColor orangeColor];
    _dailyActView.displayDataPoint = YES;
    _dailyActView.dataPointColor = [UIColor fsOrange];
    _dailyActView.dataPointBackgroundColor = [UIColor fsOrange];
    _dailyActView.dataPointRadius = 2;
    _dailyActView.color = [_dailyActView.dataPointColor colorWithAlphaComponent:0.3];
    _dailyActView.valueLabelPosition = ValueLabelLeftMirrored;
    
    _dailyActView.labelForIndex = ^(NSUInteger item) {
        return months[item];
    };
    
    _dailyActView.labelForValue = ^(CGFloat value) {
        return [NSString stringWithFormat:@"%.d", (int)value];
    };
    
    [_dailyActView setChartData:chartData];
}




@end
