//
//  PictureDetailViewController.h
//  HuntPro
//
//  Created by AOC on 25/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "CommonViewController.h"
#import "ProfilePictureEntity.h"

@interface PictureDetailViewController  : CommonViewController 
    


@property (nonatomic, strong) NSMutableArray *_picList;
@property (nonatomic, strong) ProfilePictureEntity * _selectedPicture;

@end
