//
//  PictureDetailViewController.m
//  HuntPro
//
//  Created by AOC on 25/10/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "PictureDetailViewController.h"
#import "MyPicViewController.h"

#import "CommonUtils.h"
#import "UserEntity.h"
#import "PictureEntity.h"

@import AFNetworking;

@interface PictureDetailViewController () {
    
    PictureEntity * _detailPicture;
}
@property (weak, nonatomic) IBOutlet UIImageView *imvPicture;

@end

@implementation PictureDetailViewController

@synthesize _selectedPicture;

- (void) viewDidLoad {
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.tabBarController.tabBar.hidden = YES;
    
    [self getPictureDetail];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [_imvPicture setImageWithURL:[NSURL URLWithString:_selectedPicture._blobUrl]];
    
    PICTURE_BACK = 1;
    
}

- (IBAction)closeAction:(id)sender {
    
    [self performSegueWithIdentifier:@"unwind2MyPic" sender:self];
}


- (void) getPictureDetail {///api/Gallery/Picture/{pictureId}
    
    [self showLoadingView];
    
//    NSString *url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GET_PICTURE, _selectedPicture._pictureId];

    //for test
    NSString *pictureId = @"d1da5361-1da5-4e6a-9440-f9c74953a671";
    NSString *url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GET_PICTURE, pictureId];
    
    NSLog(@"get picture url : %@", url);
    
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"get picture detail result : %@", responseObject);
        
        _detailPicture = [[PictureEntity alloc] init];
        
        _detailPicture._pictureId = [responseObject valueForKey:RES_PICTUREID];
        _detailPicture._title = [responseObject valueForKey:RES_TITLE];
        _detailPicture._description = [responseObject valueForKey:RES_DESCRIPTION];
        
        id propertyDic = [responseObject valueForKey:RES_PROPERTY];
        
        PropertyEntity *_property = [[PropertyEntity alloc] init];
        
        if (propertyDic != [NSNull null]) {
            
            NSDictionary *_propertyDic = (NSDictionary *) propertyDic;
            
            _property._propertyId = [[_propertyDic valueForKey:RES_PROPERTYID] intValue];
            _property._propertyName = [_propertyDic valueForKey:RES_PROPERTYNAME];
            _property._zipCode = [_propertyDic valueForKey:RES_ZIPCODE];
            _property._dataUsed = [[_propertyDic valueForKey:RES_DATAUSED] intValue];
        }
        
        _detailPicture._property = _property;
        
        _detailPicture._created = [responseObject valueForKey:RES_CREATED];
        _detailPicture._modified = [responseObject valueForKey:RES_MODIFIED];
        _detailPicture._taken = [responseObject valueForKey:RES_TAKEN];
        _detailPicture._size = [[responseObject valueForKey:RES_SIZE] intValue];
        _detailPicture._width = [[responseObject valueForKey:RES_WIDTH] intValue];
        _detailPicture._height = [[responseObject valueForKey:RES_HEIGHT] intValue];
        _detailPicture._blogUrl = [responseObject valueForKey:RES_BLOUURL];
        
        
        id thumbnailDic = [responseObject valueForKey:RES_THUMNAILS];
        
        if (thumbnailDic != [NSNull null]) {
            
            for (NSDictionary *_thumbnailDic in thumbnailDic) {
                
                ThumbnailEntity *_thumbnail = [[ThumbnailEntity alloc] init];
                
                //NSDictionary *_thumbnailDic = (NSDictionary *) thumbnailDic;
                
                _thumbnail._thumbnailId = [[_thumbnailDic valueForKey:RES_THUMNAILID] intValue];
                _thumbnail._blobUrl = [_thumbnailDic valueForKey:RES_BLOUURL];
                _thumbnail._width = [[_thumbnailDic valueForKey:RES_WIDTH] intValue];
                _thumbnail._height = [[_thumbnailDic valueForKey:RES_HEIGHT] intValue];
                _thumbnail._size = [[_thumbnailDic valueForKey:RES_SIZE] intValue];
                
                [_detailPicture._thumbnails addObject:_thumbnail];
            }
        
        }
        
        id tagDic = [responseObject valueForKey:RES_TAGS];
        
        if (tagDic != [NSNull null]) {
            
            for (NSDictionary *_tagDic in tagDic) {
                
                TagEntity *_tag = [[TagEntity alloc] init];
                
                _tag._accountId = [[_tagDic valueForKey:RES_ACCOUNTID] intValue];
                _tag._title = [_tagDic valueForKey:RES_TITLE];
                _tag._description = [_tagDic valueForKey:RES_DESCRIPTION];
                _tag._userAccount = [[_tagDic valueForKey:RES_USERCOUNT] intValue];
                
                [_detailPicture._tags addObject:_tag];
            }
        }
        
        id locationDic = [responseObject valueForKey:RES_LOCATION];
        
        LocationEntity *_location = [[LocationEntity alloc] init];
        
        if (locationDic != [NSNull null]) {
            
            NSDictionary *_locationDic = (NSDictionary *) locationDic;
            
            _location._id = [[_locationDic valueForKey:RES_ID] intValue];
            _location._latitude = [[_locationDic valueForKey:RES_LATITUDE] floatValue];
            _location._longitude = [[_locationDic valueForKey:RES_LONGITUDE] floatValue];
            _location._title = [_locationDic valueForKey:RES_TITLE];
            _location._ownerId = [_locationDic valueForKey:RES_OWNER_ID];
            _location._propertyId = [[_locationDic valueForKey:RES_PROPERTYID] intValue];
            _location._description = [_locationDic valueForKey:RES_DESCRIPTION];
            
        }
        
        _detailPicture._location = _location;
        
        NSDictionary * _userPermissionDic = [responseObject valueForKey:RES_USER_PERMISSION];
        
        PermissionEntity *_userPermission = [[PermissionEntity alloc] init];
        
        _userPermission._canView = ([[_userPermissionDic valueForKey:RES_CANVIEW] intValue] == 1);
        _userPermission._canEdit = ([[_userPermissionDic valueForKey:RES_CANEDIT] intValue] == 1);
        _userPermission._canShare = ([[_userPermissionDic valueForKey:RES_CANSHARE] intValue] == 1);
        _userPermission._canAdd = ([[_userPermissionDic valueForKey:RES_CANADD] intValue] == 1);
        _userPermission._canDelete = ([[_userPermissionDic valueForKey:RES_CANDELETE] intValue] == 1);
        
        id originLocatoinDic = [responseObject valueForKey:RES_ORIGINFILELOCATION];
        
        LocationEntity *_orgFileLocation = [[LocationEntity alloc] init];
        
        if (originLocatoinDic != [NSNull null]) {
            
            NSDictionary *_originLocatoinDic = (NSDictionary *) originLocatoinDic;
            
            _orgFileLocation._id = [[_originLocatoinDic valueForKey:RES_ID] intValue];
            _orgFileLocation._latitude = [[_originLocatoinDic valueForKey:RES_LATITUDE] floatValue];
            _orgFileLocation._longitude = [[_originLocatoinDic valueForKey:RES_LONGITUDE] floatValue];
            _orgFileLocation._title = [_originLocatoinDic valueForKey:RES_TITLE];
            _orgFileLocation._ownerId = [_originLocatoinDic valueForKey:RES_OWNER_ID];
            _orgFileLocation._propertyId = [[_originLocatoinDic valueForKey:RES_PROPERTYID] intValue];
            _orgFileLocation._description = [_originLocatoinDic valueForKey:RES_DESCRIPTION];
            
        }
        
        _detailPicture._originalFileLocation = _orgFileLocation;
        
        //[_imvPicture setImageWithURL:[NSURL URLWithString:_detailPicture._blogUrl]];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:@"You are not authenticated in this time" positive:ALERT_OK negative:nil];
        
    }];
    
}

@end
