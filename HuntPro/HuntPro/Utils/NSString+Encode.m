//
//  NSString+Encode.m
//  CampusGlue
//


#import "NSString+Encode.h"

@implementation NSString (encode)

- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[].",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding)));
}

@end
