//
//  NSString+Encode.h
//  CampusGlue
//

#import <Foundation/Foundation.h>

@interface NSString(encode)

-(NSString *)encodeString:(NSStringEncoding) encoding;

@end
