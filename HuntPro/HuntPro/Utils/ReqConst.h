//
//  ReqConst.h
//  HuntPro
//


#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

//developing
#define SERVER_URL                 @"https://hunt.azurewebsites.net/api/"

//product
//#define SERVER_BASE_URL                 @"https://portal.huntpros.com"


#pragma mark -
#pragma mark - APIs

/******    APIs  **********/

#define REQ_LOGIN                       @"Authenticate/Login"
#define REQ_GET_STANDS                  @"Map/ByProperty/ReservationManager"
#define REQ_CHECKIN_STAND               @"Map/Reservations/CheckIn"
#define REQ_CHECKOUT_STAND              @"Map/Reservations/CheckOut"
#define REQ_GET_LOCATION                @"Location/ByProperty"
#define REQ_GET_TAG                     @"Tags/ByAccount"
#define REQ_GET_GALLERY                 @"Gallery/Property"
#define REQ_GET_PICTURE                  @"Gallery/Picture"




#pragma mark -
#pragma mark - Response Params

/******  login   ********/

#define RES_MESSAGE                     @"Message"
#define RES_DETAILEDMESSAGE             @"DetailedMessage"
#define RES_ACTION                      @"Action"
#define RES_DETAILEDACTION              @"DetailedAction"

#define RES_ID                          @"Id"
#define RES_NAME                        @"Name"
#define RES_PROFILE_PIC                 @"ProfilePicture"
#define RES_CLAIMS                      @"Claims"
#define RES_ROLES                       @"Roles"

#define RES_ISSITEADMIN                 @"IsSiteAdmin"
#define RES_ACCOUNTS                    @"Accounts"

//accounts
#define RES_ACCOUNTID                   @"AccountId"
#define RES_ACCOUNTNAME                 @"AccountName"
#define RES_USERROLE                    @"UserRole"

//permissions
#define RES_PICTUREPERMISSION           @"PicturePermissions"
#define RES_EVENTPERMISSION             @"EventPermissions"
#define RES_ACCOUNTPERMISSION           @"AccountPermissions"
#define RES_GAMELOGPERMISSION           @"GameLogPermissions"
#define RES_MAPPERMISSION               @"MapPermissions"
#define RES_CHATPERMISSION              @"ChatPermissions"

//permission items
#define RES_CANVIEW                     @"CanView"
#define RES_CANEDIT                     @"CanEdit"
#define RES_CANSHARE                    @"CanShare"
#define RES_CANADD                      @"CanAdd"
#define RES_CANDELETE                   @"CanDelete"

//profile
#define RES_PICTUREID                   @"PictureId"
#define RES_SIZE                        @"Size"
#define RES_WIDTH                       @"Width"
#define RES_HEIGHT                      @"Height"
#define RES_BLOUURL                     @"BlobUrl"
#define RES_THUMNAILS                   @"Thumbnails"

//Thumbnails

#define RES_THUMNAILID                  @"ThumbnailID"


#define RES_MEMBERSTAT                  @"MemberStat"
#define RES_PROPERTYSTAT                @"PropertyStat"
#define RES_DATAUSED                    @"DataUsed"

#define RES_PROPERTIES                 @"Properties"

//properties
#define RES_PROPERTYID                  @"PropertyId"
#define RES_PROPERTYNAME                @"PropertyName"
#define RES_ZIPCODE                     @"ZipCode"

//Organization
#define RES_ORGANIZATION                @"Organization"
#define RES_ORGANIZATIONNAME            @"OrganizationName"
#define RES_ORGANIZATIONID              @"OrganizationId"
#define RES_ORGANIZATIONPICURL          @"OrganizationPictureUrl"
#define RES_WEBWIDGETS                  @"WebWidgets"
#define RES_WIDGETS                     @"Widgets"


//My Stands
#define RES_TITLE                       @"Title"
#define RES_LOCATIONPOINTTYPES          @"LocationPointTypes"
#define RES_RESERVATION                 @"Reservation"
#define RES_LOCATIONID                  @"LocationId"
#define RES_USER                        @"user"
#define RES_STARTTIME                   @"StartTime"
#define RES_ENDTIME                     @"EndTime"


//Location ByProperty
#define RES_LATITUDE                    @"Latitude"
#define RES_LONGITUDE                   @"Longitude"
#define RES_OWNER_ID                    @"OwnerId"
#define RES_DESCRIPTION                 @"Description"

//Tag byAccount
#define RES_USERCOUNT                   @"UseCount"


//get Picture detail by picture id
#define RES_PROPERTY                    @"Property"
#define RES_CREATED                     @"Created"
#define RES_MODIFIED                    @"Modified"
#define RES_TAKEN                       @"Taken"
#define RES_TAGS                        @"Tags"
#define RES_LOCATION                    @"Location"
#define RES_USER_PERMISSION             @"UserPermission"
#define RES_ORIGINFILELOCATION          @"OriginalFileLocation"






#pragma mark -
#pragma mark - Request Params


/*****  Login  *****/

#define PARAM_USERNAME                  @"username"
#define PARAM_PASSWORD                  @"password"

#define PARAM_TYPE                      @"Type"

#define PARAM_TAGS                      @"tags"
#define PARAM_LOCATIONS                 @"locations"
#define PARAM_TITLE                     @"title"
#define PARAM_ACCOUNTID                 @"accountId"
#define PARAM_ID                        @"id"


////////////////////////////////////////////////////


#endif /* ReqConst_h */
