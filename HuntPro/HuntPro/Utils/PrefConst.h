//
//  PrefConst.h
//  HuntPro
//


#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_ID              @"user_id"
#define PREFKEY_USERNAME        @"user_username"
#define PREFKEY_NAME            @"user_name"
#define PREFKEY_PASSWORD        @"user_password"
#define PREFKEY_LOGGED_IN       @"user_logged_in"

// for temp
#define PREFKEY_PROPERTYID      @"user_propertyid"


#endif /* PrefConst_h */
