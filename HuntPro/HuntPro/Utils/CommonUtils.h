//
//  CommonUtils.h
//  HuntPro
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "Const.h"
#import "ReqConst.h"
#import "NSString+Encode.h"
#import "UIImageResizeMagick.h"
#import "AppDelegate.h"

#import <AFNetworking/AFNetworking.h>
//#import <JLToast/JLToast-Swift.h>
//#import <Toaster/Toaster.h>

@interface CommonUtils : NSObject

+ (BOOL) isValidEmail: (NSString *) email;

// save image to file with size specification
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;

+ (void) saveUserInfo;
+ (void) loadUserInfo;
//

+ (void) setUserId : (NSString *) id;
+ (NSString *) getUserId;


+ (void) setUserName: (NSString *) username;
+ (NSString *) getUserName;

+ (void) setName: (NSString *) name;
+ (NSString *) getName;

+ (void) setUserPassword: (NSString *) password;
+ (NSString *) getUserPassword;

+ (void) setPropertyId : (int) propertyId;
+ (int) getPropertyId ;

+ (void) setUserLogin:(BOOL) isLoggedIn;
+ (BOOL) getUserLogin;

@end
