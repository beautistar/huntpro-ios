//
//  Const.h
//  HuntPro
//


#import <Foundation/Foundation.h>

@interface Const : NSObject

#define SAVE_ROOT_PATH                      @"HuntPro"

#define METER_PER_MILE 1609.344
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


/**
 **  set global variable
 **/

extern int ACCOUNTID;
extern int PROPERTYID;

extern int PICTURE_BACK;

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

//string const

#define CONN_ERROR                          @"Connect server fail.\n Please try again when you are online."

#define ALERT_TITLE                         @"HuntPro"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"
#define ALERT_CHECKOUT                      @"Check Out"
#define ALERT_CHECKIN                       @"Check In"
#define ALERT_CHECKIN_MSG                   @"To check in, click OK."
#define ALERT_CHECKOUT_MSG                  @"To check out, click OK."

#define INPUT_USERNAME                      @"Please input User Name."
#define INPUT_PWD                           @"Please input Password."

//#define INPUT_COMPANY                       @"Please input company."
//#define INPUT_PHONE1                        @"Please input phone number 1."
//#define INVALID_EDITCUSTOMER                @"Invalid to edit customer."
//#define INVALID_DELCUSTOMER                 @"Invalid to delete customer."
//#define INVALID_STYLECODE                   @"Style code never be blank."
//#define INVALID_CUSTOMER                    @"Customer never be blank."
//#define INVALID_PRICE                       @"Price never be blank."
//#define INVALID_COLOR                       @"Color never be blank"
//#define INVALID_QTY                         @"Quantity never be blank"
//#define INPUT_NOTE                          @"Input note."
//#define INPUT_MAIL                          @"Input mail"


#define FAIL_LOGIN                          @"Login failed"

//#define RES_UNREGISTERED_USERNAME           @"Unregistered username."
//#define CHECK_PWD                           @"Wrong password."
//#define EXIST_CUS_NAME                      @"Customer company or phone1 already exist."
//#define CONFIRM_CANCEL                      @"Are you sure you want to cancel order?"

@end
