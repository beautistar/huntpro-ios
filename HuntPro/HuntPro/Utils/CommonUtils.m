//
//  CommonUtils.m
//  HuntPro
//
//

#import "CommonUtils.h"
#import "PrefConst.h"
#import "UserDefault.h"

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

+ (BOOL) isValidEmail: (NSString *) email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage {
    
    return [srcImage resizedImageByMagick:@"256x256#"];
}

+ (UIImage *) imageResizeforChat: (UIImage *) scrImage {
    
    return [scrImage resizedImageByMagick:@"256x256#"];
}


// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    if(isProfile) {
        
        outputFileName = @"profile.jpg";
        outputImage = [CommonUtils imageResize:srcImage];
        
    }
    else {
        
        outputFileName = @"chatfile.jpg";
        outputImage = [CommonUtils imageResizeforChat:srcImage];
    }
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    //    if(isProfile) {
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    //    } else {
    //        [UIImageJPEGRepresentation(outputImage, 0.8) writeToFile:filePath atomically:YES];
    //    }
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}


+ (void) saveUserInfo {
    
    [CommonUtils setUserId          :APPDELEGATE.Me._id];
    [CommonUtils setName            :APPDELEGATE.Me._name];
    [CommonUtils setUserName        :APPDELEGATE.Me._username];
    [CommonUtils setUserPassword    :APPDELEGATE.Me._password];
    [CommonUtils setPropertyId      :APPDELEGATE.Me._propertyId];
}

+ (void) loadUserInfo {
    
    if(APPDELEGATE.Me) {
        
        APPDELEGATE.Me._id          = [CommonUtils getUserId];
        APPDELEGATE.Me._username    = [CommonUtils getUserName];
        APPDELEGATE.Me._name        = [CommonUtils getName];
        APPDELEGATE.Me._password    = [CommonUtils getUserPassword];
        APPDELEGATE.Me._propertyId  = [CommonUtils getPropertyId];

    }
}

+ (void) setUserId:(NSString *) idx {
    
    [UserDefault setStringValue:PREFKEY_ID value:idx];
}

+ (NSString *) getUserId {
    
    return [UserDefault getStringValue:PREFKEY_ID];
}

+ (void) setUserName: (NSString *) username {
    
    [UserDefault setStringValue:PREFKEY_USERNAME value:username];
}

+ (NSString *) getUserName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_USERNAME];
    return res == nil ? @"" : res;
}

+ (void) setName: (NSString *) name {
    
    [UserDefault setStringValue:PREFKEY_NAME value:name];
}

+ (NSString *) getName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_NAME];
    return res == nil ? @"" : res;
}

+ (void) setUserPassword: (NSString *) password {
    
    [UserDefault setStringValue:PREFKEY_PASSWORD value:password];
}

+ (NSString *) getUserPassword {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_PASSWORD];
    return res == nil ? @"" : res;
}

+ (void) setPropertyId : (int) propertyId {
    
    [UserDefault setIntValue:PREFKEY_PROPERTYID value:propertyId];
}

+ (int) getPropertyId {
    
    return [UserDefault getIntValue:PREFKEY_PROPERTYID];
    
}

+ (void) setUserLogin:(BOOL) isLoggedIn {
    
    [UserDefault setBoolValue:PREFKEY_LOGGED_IN value:isLoggedIn];
}

+ (BOOL) getUserLogin {
    
    return [UserDefault getBoolValue:PREFKEY_LOGGED_IN];
}

@end
