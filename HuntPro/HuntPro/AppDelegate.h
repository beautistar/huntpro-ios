//
//  AppDelegate.h
//  HuntPro
//
//  Created by AOC on 28/09/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity *Me;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UserEntity * Me;

@end

